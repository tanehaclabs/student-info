package com.example.taneha.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.entity.NavigationDrawerItem;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by taneha on 26-02-2015.
 */
public class NavigationDrawerAdapter extends BaseAdapter implements Serializable {
    private Context context;
    private ArrayList<NavigationDrawerItem> navDrawerItems;

    public NavigationDrawerAdapter(Context context, ArrayList<NavigationDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.nav_drawer_list, null);

        TextView itemName = (TextView) view.findViewById(R.id.list_text);
        itemName.setText(navDrawerItems.get(position).getTitle());

        ImageView itemIcon = (ImageView) view.findViewById(R.id.list_icon);
        itemIcon.setImageResource(navDrawerItems.get(position).getIcon());

        return view;
    }
}