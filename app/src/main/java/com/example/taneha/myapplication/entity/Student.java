package com.example.taneha.myapplication.entity;

public class Student {
    public String getRollnum() {
        return rollnum;
    }

    public void setRollnum(String rollnum) {
        this.rollnum = rollnum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String rollnum;
    public String name;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String address;

    public Student(String rollnum, String name, String address) {
        this.name = name;
        this.rollnum = rollnum;
        this.address = address;

    }

}


