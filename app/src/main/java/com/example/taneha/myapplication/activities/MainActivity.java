package com.example.taneha.myapplication.activities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.adapter.TabPagerAdapter;
import com.example.taneha.myapplication.adapter.TestAdapter;
import com.example.taneha.myapplication.constant.AppConstants;
import com.example.taneha.myapplication.entity.Student;
import com.example.taneha.myapplication.fragment.NavigationDrawerFragment;
import com.example.taneha.myapplication.fragment.SpinnerFragment;
import com.example.taneha.myapplication.util.DbController;
import com.example.taneha.myapplication.util.SharedPref;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainActivity extends ActionBarActivity implements SpinnerFragment.OnPass,
        ActionBar.TabListener, AppConstants, NavigationDrawerFragment.NavigationDrawerCallbacks {

    List<Student> data;
    public TestAdapter adapter;
    private ViewPager viewPager;
    private ActionBar actionBar;
    private String[] tabs = {LIST, GRID};
    private TabPagerAdapter tabPagerAdapter;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new ArrayList<>();
        adapter = new TestAdapter(this, data);
        new LoadSomeStuff().execute(null, VIEW);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabPagerAdapter);
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Save
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(NAME);
                String roll = data.getStringExtra(ROLL);
                String address = data.getStringExtra(ADDRESS);
                adapter.data.add(new Student(roll, name, address));
                adapter.notifyDataSetChanged();

            } else if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }

        //Edit
        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(NAME);
                String roll = data.getStringExtra(ROLL);
                String address = data.getStringExtra(ADDRESS);
                int position = data.getIntExtra(POSITION, 0);
                adapter.data.get(position).name = name;
                adapter.data.get(position).rollnum = roll;
                adapter.data.get(position).address = address;
                adapter.notifyDataSetChanged();

            } else if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }

    }
    @Override
    public void onPass(Bundle extras) {
        if (extras.getInt(POSITION) == 0) {
            Collections.sort(adapter.data, new Comparator<Student>() {
                @Override
                public int compare(Student lhs, Student rhs) {

                    return lhs.getName().compareToIgnoreCase(rhs.getName());
                }
            });
        } else if (extras.getInt(POSITION) == 1) {
            Collections.sort(adapter.data, new Comparator<Student>() {

                @Override
                public int compare(Student lhs, Student rhs) {
                    if (Integer.parseInt(lhs.getRollnum()) > Integer.parseInt(rhs.getRollnum()))
                        return 1;
                    else
                        return -1;
                }
            });
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    // Restoring Action Bar
    public void restoreActionBar() {
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.home);
                viewHome();
                break;
            case 2:
                mTitle = getString(R.string.add_student);
                openAddStudent();
                break;
            case 3:
                mTitle = getString(R.string.logout);
                logout();
                break;
            default:
                break;
        }
    }

    void viewHome(){
        adapter.notifyDataSetChanged();

    }

    void openAddStudent() {
        Intent intent = new Intent(MainActivity.this, AddStudent.class);
        intent.putExtra(BUTTON_CLICKED, ADD_STUDENT);
        startActivityForResult(intent, ADD_REQUEST_CODE);// Activity is started with requestCode = ADD_REQUEST_CODE

    }

    private void logout() {
        Intent intent = new Intent(MainActivity.this, SharedPref.class);
        startActivity(intent);
        finish();
    }


    public class LoadSomeStuff extends AsyncTask<String, Integer, Integer> {
        ProgressDialog progressBar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(MainActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Please Wait..");
            progressBar.setIndeterminate(false);
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            for (int i = 0; i < 101; i++) {
                try {
                    Thread.sleep(10);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (params[1].equals(VIEW)) {
                DbController controller = new DbController(getApplicationContext());
                controller.open();
                adapter.data = controller.getData();
                controller.close();
                return 1;
            } else if (params[1].equals(DELETE)) {
                DbController con = new DbController(MainActivity.this);
                con.open();
                con.DeleteEntry(params[0]);
                con.close();
                return 2;

            }
            return -1;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);

        }

        @Override
        protected void onPostExecute(Integer s) {
            progressBar.dismiss();
            if (s == 1) {
                Toast.makeText(MainActivity.this, "Data fetched!!", Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
            } else if (s == 2) {
                Toast.makeText(MainActivity.this, "Data Deleted!!", Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
            } else {
                Toast.makeText(MainActivity.this, "Nothing Done!!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
