package com.example.taneha.myapplication.constant;

/**
 * Created by taneha on 02-03-2015.
 */
public interface AppConstants {
    String BUTTON_CLICKED = "Button";
    String ADD_STUDENT = "addButton";
    int ADD_REQUEST_CODE = 2;
    String VIEW = "VIEWALL";
    String LIST = "LIST VIEW";
    String GRID = "GRID VIEW";
    String NAME = "name";
    String ROLL = "roll";
    String ADDRESS = "address";
    String POSITION = "position";
    String DELETE = "DELETE";
    String SORTNAME="SORT BY : Name";
    String SORTROLL="SORT BY : RollNum";
}