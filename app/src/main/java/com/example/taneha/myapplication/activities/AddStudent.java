package com.example.taneha.myapplication.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.constant.AppConstants;
import com.example.taneha.myapplication.util.DbController;


public class AddStudent extends Activity implements AppConstants{
    DbController controller;

    EditText editName;
    EditText editRollNum;
    EditText editAddress;
    Button saveButton;
    Button cancelButton;
    boolean success = true;
    boolean editCase = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        editName = (EditText) findViewById(R.id.edit_name);
        editRollNum = (EditText) findViewById(R.id.edit_roll_num);
        editAddress = (EditText) findViewById(R.id.edit_address);
        saveButton = (Button) findViewById(R.id.save_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        if (getIntent().hasExtra(POSITION)) {
            editName.setText(getIntent().getStringExtra(NAME));
            editRollNum.setText((getIntent().getStringExtra(ROLL)));
            editAddress.setText((getIntent().getStringExtra(ADDRESS)));
            editCase = true;
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    String messageName = editName.getText().toString();
                    String messageRollNum = editRollNum.getText().toString();
                    String messageAddress = editAddress.getText().toString();
                    controller = new DbController(AddStudent.this);
                    controller.open();
                    controller.writeInDatabase(messageRollNum, messageName, messageAddress);
                    controller.close();
                    Intent intent = new Intent();
                    intent.putExtra(NAME, messageName);
                    intent.putExtra(ROLL, messageRollNum);
                    intent.putExtra(ADDRESS, messageAddress);

                    if (editCase == true) {
                        intent.putExtra(POSITION, getIntent().getIntExtra(POSITION, 0));
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                } catch (Exception e) {
                    success = false;
                    Toast.makeText(getApplicationContext(), "DETAILS UNSAVED..ERROR", Toast.LENGTH_SHORT).show();

                } finally {
                    if (success) {
                        Toast.makeText(getApplicationContext(), "DETAILS ADDED", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

}






