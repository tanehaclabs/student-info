package com.example.taneha.myapplication.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.taneha.myapplication.constant.DbConstants;
import com.example.taneha.myapplication.entity.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taneha on 05-02-2015.
 */
public class DbController implements DbConstants {

    DbHelper ourHelper;
    public final Context ourContext;
    private SQLiteDatabase ourDatabase;

    public DbController(Context c) {
        ourContext = c;
    }

    public void open() {
        ourHelper = new DbHelper(ourContext);
        ourDatabase = ourHelper.getWritableDatabase();

    }

    public void close() {
        ourHelper.close();
    }

    public long writeInDatabase(String messageRollNum, String messageName, String messageAddress) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ROLL, messageRollNum);
        cv.put(KEY_NAME, messageName);
        cv.put(KEY_ADDRESS, messageAddress);
        return ourDatabase.insert(TABLE_NAME, null, cv);
    }

    public List<Student> getData() {
        String[] columns = new String[]{KEY_ROLL, KEY_NAME, KEY_ADDRESS};
        Cursor c = ourDatabase.query(TABLE_NAME, columns, null, null, null, null, null);

        int iRoll = c.getColumnIndex(KEY_ROLL);
        int iName = c.getColumnIndex(KEY_NAME);
        int iAddress = c.getColumnIndex(KEY_ADDRESS);
        List<Student> students = new ArrayList<>();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            students.add(new Student(c.getString(iRoll), c.getString(iName), c.getString(iAddress)));
        }
        return students;
    }


    public void DeleteEntry(String str) {

        ourDatabase.delete(TABLE_NAME, KEY_ROLL + "=?", new String[]{str});
    }
}
