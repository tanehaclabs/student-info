package com.example.taneha.myapplication.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.activities.AddStudent;
import com.example.taneha.myapplication.activities.MainActivity;
import com.example.taneha.myapplication.adapter.TestAdapter;
import com.example.taneha.myapplication.constant.AppConstants;
import com.example.taneha.myapplication.util.DisplayInfo;


public class ListFragment extends Fragment implements AppConstants {
    ListView listView;
    TestAdapter adapter;

    @Nullable

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listview, container, false);
        listView = (ListView) view.findViewById(R.id.list);
        adapter = ((MainActivity) getActivity()).adapter;
        listView.setAdapter(((MainActivity) getActivity()).adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Please Choose!!");
                Button viewButton = (Button) dialog.findViewById(R.id.ButtonView);
                viewButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), DisplayInfo.class);
                        intent.putExtra(NAME, adapter.data.get(position).name);
                        intent.putExtra(ROLL, adapter.data.get(position).rollnum);
                        intent.putExtra(ADDRESS, adapter.data.get(position).address);
                        dialog.dismiss();
                        startActivity(intent);

                    }
                });


                Button deleteButton = (Button) dialog.findViewById(R.id.ButtonDelete);
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String pass = adapter.data.get(position).rollnum;
                        adapter.data.remove(position);
                        ((MainActivity) getActivity()).new LoadSomeStuff().execute(pass, DELETE);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });


                Button editButton = (Button) dialog.findViewById(R.id.ButtonEdit);
                editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), AddStudent.class);
                        intent.putExtra(NAME, adapter.data.get(position).name);
                        intent.putExtra(ROLL, adapter.data.get(position).rollnum);
                        intent.putExtra(ADDRESS, adapter.data.get(position).address);
                        intent.putExtra(POSITION, position);
                        getActivity().startActivityForResult(intent, 200);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return view;
    }
}
