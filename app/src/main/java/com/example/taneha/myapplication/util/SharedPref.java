package com.example.taneha.myapplication.util;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.activities.MainActivity;


public class SharedPref extends Activity implements View.OnClickListener {

    Button submit, exit;
    String userName, password;
    EditText userInput, passInput;
    android.content.SharedPreferences.Editor toEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);
        submit = (Button) findViewById(R.id.submit);
        exit = (Button) findViewById(R.id.exit);
        userInput = (EditText) findViewById(R.id.user_input);
        passInput = (EditText) findViewById(R.id.pass_input);
        submit.setOnClickListener(this);
        exit.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.submit:
                SharedPreferences shf = getSharedPreferences("LOGIN CREDENTIALS", MODE_PRIVATE);
                String getData = shf.getString("USERNAME", null);
                String getPass = shf.getString("PASSWORD", null);


                if (getData == null) {
                    userName = userInput.getText().toString();
                    password = passInput.getText().toString();
                    toEdit = shf.edit();
                    toEdit.putString("USERNAME", userName);
                    toEdit.putString("PASSWORD", password);
                    toEdit.commit();
                    Toast.makeText(this, "Details are saved", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SharedPref.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    userName = userInput.getText().toString();
                    password = passInput.getText().toString();
                    if ((userName.equals(getData)) && (password.equals(getPass))) {
                        Toast.makeText(this, "Logged In Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SharedPref.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(this, "Wrong Password..", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                break;

            case R.id.exit:

                finish();
                break;

        }


    }
}