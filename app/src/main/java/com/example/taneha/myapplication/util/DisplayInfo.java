package com.example.taneha.myapplication.util;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.constant.AppConstants;


public class DisplayInfo extends Activity implements AppConstants {
    TextView name;
    TextView showName;
    TextView roll;
    TextView showRoll;
    TextView address;
    TextView showAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displayinfo);
        name = (TextView) findViewById(R.id.name_text);
        showName = (TextView) findViewById(R.id.name_view);
        roll = (TextView) findViewById(R.id.roll_text);
        showRoll = (TextView) findViewById(R.id.roll_view);
        address = (TextView) findViewById(R.id.address_text);
        showAddress = (TextView) findViewById(R.id.address_view);

        showName.setText(getIntent().getStringExtra(NAME));
        showRoll.setText(getIntent().getStringExtra(ROLL));
        showAddress.setText(getIntent().getStringExtra(ADDRESS));

    }

}