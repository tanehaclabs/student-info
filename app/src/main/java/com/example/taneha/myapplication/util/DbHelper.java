package com.example.taneha.myapplication.util;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.taneha.myapplication.constant.DbConstants;


public class DbHelper extends SQLiteOpenHelper implements DbConstants {

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = " CREATE TABLE " + TABLE_NAME + "(" + KEY_ROLL + " VARCHAR(10) PRIMARY KEY, " + KEY_NAME + "  VARCHAR(20)," + KEY_ADDRESS + " VARCHAR(30) " + " ); ";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        onCreate(db);
    }
}
