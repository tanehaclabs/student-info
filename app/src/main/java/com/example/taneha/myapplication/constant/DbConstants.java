package com.example.taneha.myapplication.constant;

/**
 * Created by taneha on 05-02-2015.
 */
public interface DbConstants {
    String DATABASE_NAME = "studentDatabase";
    static final String TABLE_NAME = "studentTable";
    static final int DATABASE_VERSION = 1;
    static final String UID = "_id";
    static final String KEY_NAME = "name";
    static final String KEY_ADDRESS = "address";
    static final String KEY_ROLL = "rollNum";

}
