package com.example.taneha.myapplication.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.constant.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class SpinnerFragment extends Fragment implements AppConstants{
    private Spinner spinner1;
    Bundle extras = new Bundle();
    OnPass pass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_spinner, container, false);
        spinner1 = (Spinner) view.findViewById(R.id.spinner);
        List<String> list = new ArrayList<String>();
        list.add(SORTNAME);
        list.add(SORTROLL);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item, list);
        spinner1.setAdapter(dataAdapter);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    extras.putInt(POSITION, position);
                    pass.onPass(extras);

                }
                if (position == 1) {
                    extras.putInt(POSITION, position);
                    pass.onPass(extras);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    public interface OnPass

    {
        public void onPass(Bundle extras);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            pass = (OnPass) activity;
        } catch (ClassCastException c) {
            throw new ClassCastException(activity.toString() + "must implement onPass");
        }
    }
}