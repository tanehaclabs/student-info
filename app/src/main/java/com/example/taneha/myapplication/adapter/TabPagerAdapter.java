package com.example.taneha.myapplication.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.taneha.myapplication.fragment.GridFragment;
import com.example.taneha.myapplication.fragment.ListFragment;

/**
 * Created by taneha on 25-02-2015.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        switch (position) {
            case 0:
                ListFragment listFragment = new ListFragment();
                return listFragment;
            case 1:
                GridFragment gridFragment = new GridFragment();
                return gridFragment;
        }
        return null;
    }
}
