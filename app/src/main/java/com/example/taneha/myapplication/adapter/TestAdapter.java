package com.example.taneha.myapplication.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.taneha.myapplication.R;
import com.example.taneha.myapplication.entity.Student;

import java.io.Serializable;
import java.util.List;

public class TestAdapter extends BaseAdapter implements Serializable {

    public List<Student> data;
    Context ctx;


    public TestAdapter(Context ctx, List<Student> data) {
        this.ctx = ctx;
        this.data = data;

    }

    @Override
    public int getCount() {

        return data.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.singlerow, null);
        TextView viewName = (TextView) v.findViewById(R.id.name);
        TextView viewRoll = (TextView) v.findViewById(R.id.roll_num);
        TextView viewAddress = (TextView) v.findViewById(R.id.address);
        viewName.setText(data.get(position).name);
        viewRoll.setText(data.get(position).rollnum);
        viewAddress.setText(data.get(position).address);
        return v;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }
}
